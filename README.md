# Manual

## (a) Fork repository and clone the fork

    git clone git@bitbucket.org:hon3y/hny_project_fork.git

## (b) Clone repository, remove .git or change remote

    git clone git@bitbucket.org:hon3y/hny_project.git hny_project_x
    
    # do ...
    rm -rf ./hny_project_x/.git
    git init ./hny_project_x
    # ... or
    git remote set-url origin <new origin url>

## Start docker    
    
    # on arch linux
    sudo docker systemctl start docker.service

## Change into directory 'local'

    cd ./local

## Replace <yymmddhhmm>_<projectname> placeholders in .env

    The value must be unique. Otherwise multiple projects override each other.

## Build

    # on arch linux
    sudo docker-compose up -d
    # on osx
    docker-compose up -d

## Rebuild

    # on arch linux
    sudo make rebuild
    # on osx
    make rebuild

## Change into...

### ... app

    # on arch linux
    sudo make root
    # on osx
    make root

### ... gulp

    # on arch linux
    sudo make gulp
    # on osx
    make gulp

#### Start gulp inside container

    $ gulp
    

